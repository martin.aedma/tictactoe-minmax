﻿using System;

namespace Harj06
{
    class Utils
    {
        public static void Info()
        {
            Console.WriteLine("Utils.Info");
        }

        public static void GetTwoNumbers ()
        {
            Console.WriteLine("Tämä ohjelma kysy sinulta kaksi numeroa ja tulosta niiden eron");
            Console.WriteLine("Kirjoita ensimmäinen numero: ");
            string num1 = Console.ReadLine();
            int number1 = Convert.ToInt16(num1);
            Console.WriteLine("");
            Console.WriteLine("Kirjoita toinen numero: ");
            string num2 = Console.ReadLine();
            int number2 = Convert.ToInt16(num2);
            Console.WriteLine("");
            Console.WriteLine("Antamsi kahden numeron ero on: ");
            Console.WriteLine(Utils.Subtract(number1,number2));
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin alkaa seuraava ohjelma");
            Console.ReadKey();
            Console.Clear();
        }

        public static int Subtract (int number1, int number2)
        {
            int number3 = number1 - number2;
            return number3;
            
        }

        public static void GetThreeNumbers()
        {
            Console.WriteLine("Tämä ohjelma kysy sinulta kolme numeroa ja tulosta niiden keskiarvon");
            Console.WriteLine("Kirjoita ensimmäinen numero: ");
            string num1 = Console.ReadLine();
            int number1 = Convert.ToInt16(num1);
            Console.WriteLine("");
            Console.WriteLine("Kirjoita toinen numero: ");
            string num2 = Console.ReadLine();
            int number2 = Convert.ToInt16(num2);
            Console.WriteLine("");
            Console.WriteLine("Kirjoita kolmas numero: ");
            string num3 = Console.ReadLine();
            int number3 = Convert.ToInt16(num3);
            Console.WriteLine("");
            Console.WriteLine("Antamsi kolmen numeron keskiarvo on: ");
            Console.WriteLine(Utils.Average(number1, number2,number3));
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin alkaa seuraava ohjelma");
            Console.ReadKey();
            Console.Clear();
        }

        public static decimal Average (int number1, int number2, int number3)
        {
            decimal average = Decimal.Divide ((number1 + number2 + number3), 3);
            return average;
        }

        public static void CalConsumption (double litersPerKm, double pricePerLiter, double kilometers)
        {
            double fuelUsed = (litersPerKm * kilometers) / 100;
            double fuelPrice = fuelUsed * pricePerLiter;
            Console.WriteLine("Autosi käytti " + kilometers + " km matkaan " + fuelUsed + " litra polttoainetta.");
            Console.WriteLine("");
            Console.WriteLine("Tämän matkan verran polttainetta maksaa yhteensä " + fuelPrice + " Euroa");
        }

        public static void RequiredInfo ()
        {
            Console.WriteLine("");
            Console.WriteLine("Tämä ohjelma kysy sinulta autosi: ");
            Console.WriteLine("Polttoaine kulutuksen (montako litra per 100 km)");
            Console.WriteLine("Polttoaine hinnan per litra Euroissa");
            Console.WriteLine("Matkan pituuten kilometreissa");
            Console.WriteLine("");
            Console.WriteLine("Sen jälkeen tulosta käytetyn polttoaine määrän ja sen kustannuksen");
            Console.WriteLine("");
            Console.WriteLine("Mikä on autosi polttoaine kulutus - litra / 100 km");
            string liters = Console.ReadLine();
            double litersPerKm = Convert.ToDouble(liters);
            Console.WriteLine("");
            Console.WriteLine("Mikä on autosi käyttämän polttoainen hinta per litra?");
            string price = Console.ReadLine();
            double pricePerLiter = Convert.ToDouble(price);
            Console.WriteLine("");
            Console.WriteLine("Kuinka pitkä on matka (kilometreissa)?");
            string kilometers = Console.ReadLine();
            double tripKm = Convert.ToDouble(kilometers);

            Utils.CalConsumption(litersPerKm, pricePerLiter, tripKm);
        }
    }
}

