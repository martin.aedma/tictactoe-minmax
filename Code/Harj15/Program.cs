﻿using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace Harj15

{
    class Program
    {
        static void Main(string[] args)
        {
            ReadFile(WriteText("UserText.txt"));

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin alkaa seuraava ohjelma");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Yritän seuraavaksi avata sinu käyttäjän MyDocuments kansiosta tiedostoa nimelta Names.txt");
            Console.WriteLine("Jos semmonen tiedosto on niin rupeen työskentele sitä, muuten ilmoitan että sitä ei ole");
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin aloitan");
            Console.ReadKey();

            Names("Names.txt");

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin alkaa numeroiden ohjelma");
            Console.ReadKey();

            Numbers("Whole.txt", "Float.txt");

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin alkaa TV ohjelma");
            Console.ReadKey();
            Console.Clear();

            TvGuide("Tvohjelma.txt");

            Console.WriteLine("Ohjelma loppui!");
            Console.ReadKey();
        }

        public static string WriteText(string text)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path = Path.Combine(path, text);
            bool check = false;

            do
            {
                if (!File.Exists(path))
                {
                    Console.WriteLine("Kirjota tekstia, tallennan sen tiedostoon nimellä UserText, sinun MyDocuments kansioon");
                    Console.WriteLine("Aloita: ");
                    string addText = Console.ReadLine();
                    string[] userText = new string[1];
                    userText[0] = addText;
                    File.WriteAllLines(path, userText, Encoding.UTF8);

                    Console.Clear();
                    Console.WriteLine("Haluatko lisätä vielä tekstia?");
                    Console.WriteLine("Paina ENTER jos haluat, paina jotain muuta jos ei");

                    if (Console.ReadKey().Key != ConsoleKey.Enter)
                    {
                        return path;
                    }
                }

                Console.Clear();
                Console.WriteLine("Kirjota tekstia, tallennan sen tiedostoon nimellä UserText, sinun MyDocuments kansioon");
                Console.WriteLine("Aloita: ");
                string appendText = Console.ReadLine() + Environment.NewLine;
                File.AppendAllText(path, appendText, Encoding.UTF8);

                Console.Clear();
                Console.WriteLine("Haluatko lisätä vielä tekstia?");
                Console.WriteLine("Paina ENTER jos haluat, paina jotain muuta jos ei");

                if (Console.ReadKey().Key != ConsoleKey.Enter)
                {
                    return path;
                }

            } while (check == false);

            return path;

        }

        public static void ReadFile(string path)
        {
            Console.Clear();
            Console.WriteLine("Olet sisäistänyt tämän tekstin tiedostoon:");
            Console.WriteLine("");
            string[] readText = File.ReadAllLines(path, Encoding.UTF8);
            foreach (string s in readText)
            {
                Console.WriteLine(s);
            }
        }

        public static void Names(string text)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path = Path.Combine(path, text);

            if (File.Exists(path))
            {
                string[] readText = File.ReadAllLines(path, Encoding.UTF8);
                Console.WriteLine("Tiedostossa on " +readText.Length+ " nimeä");

                Console.WriteLine("");

                var query = readText
               .GroupBy(x => x)
               .Where(g => g.Count() > 1)
               .ToDictionary(x => x.Key, y => y.Count());

                foreach (KeyValuePair<string, Int32> kvp in query)
                {
                    Console.WriteLine("Nimi = {0}, Toistu, kerta = {1}", kvp.Key,kvp.Value);
                }
                
                Console.WriteLine("");

                SortedDictionary<string, Int32> sortedQuery = new SortedDictionary<string, Int32>(query);
                Console.WriteLine("Nimet aakosjärjestyksessä:");

                Array.Sort(readText, StringComparer.InvariantCulture);

                Array.ForEach(readText, x => Console.WriteLine(x));

            } else
            {
                Console.WriteLine("Polku");
                Console.WriteLine(path);
                Console.WriteLine("Ei ole tietostoa");
            }
        }

        public static void Numbers (string one, string two)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string pathWhole = Path.Combine(path, one);
            string pathFloat = Path.Combine(path, two);
            bool loopCheck = true;

            do
            {

                Console.Clear();
                Console.WriteLine("Sisäistä numero, kokonais tai liukuluku: ");
                string input = Console.ReadLine();

                int wholeNum;
                float floatNum;
                bool wholeCheck = int.TryParse(input, out wholeNum);
                bool floatCheck = float.TryParse(input, out floatNum);

                if (wholeCheck == true)
                {
                    using (StreamWriter writer = new StreamWriter(pathWhole, true))
                    {
                        writer.WriteLine(wholeNum);
                    }

                    loopCheck = true;

                }
                else if (floatCheck == true)
                {
                    using (StreamWriter writer = new StreamWriter(pathFloat, true))
                    {
                        writer.WriteLine(floatNum);
                    }

                    loopCheck = true;

                }
                else
                {
                    Console.WriteLine("");
                    Console.WriteLine("Et sisäistänyt kokonais - eikä liukulukua, ohjelma päätty.");
                    Console.WriteLine("Kaikki sisäistämäsi luvut löydät:");
                    Console.WriteLine("Kokonaisluvut: kansio MyDocuments, tiedosto Whole.txt");
                    Console.WriteLine("Liukuluvut: kansio MyDocuments, tiedosto Flotat.txt");

                    loopCheck = false;
                }

            } while (loopCheck == true);
        }

        public static void TvGuide (string text)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            path = Path.Combine(path, text);
            bool loopCheck = true;

            do
            {
                var singleProgram = new List<string>();

                string name, channel, time, info;
                Console.WriteLine("Sisäistä Tv ohjelman nimi: ");
                singleProgram.Add(name = Console.ReadLine());
                Console.WriteLine("Sisäistä Tv ohjelman kanava: ");
                singleProgram.Add(channel = Console.ReadLine());
                Console.WriteLine("Sisäistä Tv ohjelman kelloaika alku-loppu: ");
                singleProgram.Add(time = Console.ReadLine());
                Console.WriteLine("Sisäistä Tv ohjelman lisätiedot: ");
                singleProgram.Add(info = Console.ReadLine() + Environment.NewLine);

                string[] aProgram = singleProgram.ToArray();
                File.AppendAllLines(path, aProgram, Encoding.UTF8);
                


                Console.Clear();
                Console.WriteLine("Ohjelma");
                Console.WriteLine("");
                foreach (string t in aProgram)
                {
                    Console.WriteLine(t);
                }

                Console.WriteLine("");
                Console.WriteLine("On tallennettu");
                Console.WriteLine("");
                Console.WriteLine("Haluatko lisätä uuden ohjelman?");
                Console.WriteLine("Paina ENTER jos haluat, paina jotain muuta jos ei");


                if (Console.ReadKey().Key != ConsoleKey.Enter)
                {
                    loopCheck = false;
                }

                Console.Clear();

            } while (loopCheck == true);

            Console.Clear();
            Console.WriteLine("Tv ohjelmat (MyDocuments/Tvohjelma.txt): ");
            Console.WriteLine("");
            string[] readText = File.ReadAllLines(path, Encoding.UTF8);
            foreach (string s in readText)
            {
                Console.WriteLine(s);
            }
        }

        

    }
}
