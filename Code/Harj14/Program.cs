﻿using System;

namespace Harj14
{
    class Program
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine("Tämä ohjelma alkaa sillä, että yritän korotta integer muutuja");
            Console.WriteLine("yhden verran. Mutta sillä muutujalla on jo max arvo!!!");
            Console.WriteLine("Huomaat että ohjelma ei kaatu koska käytän try / catch ");
            Console.WriteLine("yhdistelmä.");
            Console.WriteLine("Paina jotain näppäintä niin ohjelma alkaa: int.MaxValue + int.Parse(1) = ?");
            Console.ReadKey();
            Overflow();
            Console.Clear();
            Usertxt(User());
            Console.WriteLine("");
            Console.WriteLine("Tämän ohjelman viimeinen vaihe on heittää sinulle yksi");
            Console.WriteLine("NotImplementedException virhe, kutsumalla metodia");
            Console.WriteLine("Paina jotain näppäintä niin näet sen virhen ja ohjelma loppuu.");
            Console.WriteLine("Kiitos!");
            Console.ReadKey();
            Method();
            
        }

        public static void Overflow ()
        {

            checked
            {
                try
                {
                    int value = int.MaxValue + int.Parse("1");
                    Console.WriteLine(value);
                    Console.ReadKey();
                } catch
                {
                    Console.WriteLine("Muuttuja liian iso ollakseen int!");
                    Console.WriteLine("Paina jotain näppäintä niin jatketaan");
                    Console.ReadKey();
                }
                
            }
        }

        public static int Method()
        {
            
            throw new NotImplementedException();
        }

        public static byte User()
        {
            Console.WriteLine("Taulukossa on 5 vapaa paikkaa, sisäistä numero 1 ja 5 välillä");
            Console.WriteLine("(1 ja 5 mukaan lukien). Tallennan sinun tekstin antamasi paikkaan: ");

            bool check;
            byte spot;
            
            do
            {

                try
                {
                    spot = Convert.ToByte(Console.ReadLine());
                    if (spot > 5)
                    {
                        Console.Clear();
                        Console.WriteLine("Numero liian iso: pitää olla 1-5 välillä");
                        Console.WriteLine("Kokeile uudelleen: ");
                        check = false;
                    }
                    else if (spot < 1)
                    {
                        Console.Clear();
                        Console.WriteLine("Nolla ei käy: pitää olla 1-5 välillä");
                        Console.WriteLine("Kokeile uudelleen: ");
                        check = false;
                    }
                    else
                    {
                        check = true;
                        spot--;
                        return spot;
                    }
                }
                catch
                {
                    Console.Clear();
                    Console.WriteLine("Et sisäistänyt numeroa 1-5 välillä:");
                    Console.WriteLine("Kokeile uudelleen: ");
                    check = false;
                }

            } while (check == false);

            spot = 1;
            return spot;

        }

         public static void Usertxt (byte num)
        {
            Console.Clear();
            Console.WriteLine("Sisäistä teksti mitä haluat tallentaa: ");
            string txt = Console.ReadLine();

            string[] userInput = new string[5];

            userInput[num] = txt;
            num++;
            Console.WriteLine("Tekstisi on tallennettu taulukkoon " + num + " lohkoon");
        }



    }
}
