﻿using System.Collections.Generic;
using System;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Harj13
{
    class Program
    {

        static void Main(string[] args)
        {


            Dictionary<int, string> cards = new Dictionary<int, string>();

            
            int i, j, k;
            k = 0;

            for (i = 1; i < 5; i++)
            {
                for (j=2; j<15; j++)
                {
                    k++;
                    cards.Add(k, AddCards(i,j));
                }
            }


            foreach (KeyValuePair<int, string> kvp in cards)
            {
                Console.WriteLine(kvp.Key + "   " + kvp.Value);
            }
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin teen Shuffle kortteille: ");
            Console.ReadKey();
            Console.Clear();

            Shuffle(cards);
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin alkaa kassanvirran simulointi!");
            Console.WriteLine("");
            Console.ReadKey();
            Console.Clear();

            Shop();
            Movies();

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin teen saman homman Dictionary kanssa");
            Console.ReadKey();
            Console.Clear();

            MoviesDict();
            Console.WriteLine("");
            Console.WriteLine("Ohjelma loppui! Kiitos! Dictionary oli PALJON NOPEAMPI kun Lista.");
            Console.ReadKey();

            /* Listan tekeminen verratuna Dictionaryn oli PALJON hitaampi! List meni noin 350ms
             muuta Dictionary tyylin 17 ms.
             1000 elokuvan haku oli myös Listalla paljon hitaampi. Listalla noin 900ms ja Dictionary
             about 450 ms.
             Pohtinta: Dictionary tuntuu järkevämpi ratkaisu!
            */
        }

        public static string AddCards (int num1, int num2)
        {
            string ident = "";
            if (num1 == 1)
            {
                ident = "Spades";
            } else if (num1 == 2)
            {
                ident = "Clubs";
            } else if (num1 == 3)
            {
                ident = "Hearts";
            } else
            {
                ident = "Diamonds";
            }

            switch (num2)
            {
                case 2:
                    return "2 of " + ident;
                case 3:
                    return "3 of " + ident;
                case 4:
                    return "4 of " + ident;
                case 5:
                    return "5 of " + ident;
                case 6:
                    return "6 of " + ident;
                case 7:
                    return "7 of " + ident;
                case 8:
                    return "8 of " + ident;
                case 9:
                    return "9 of " + ident;
                case 10:
                    return "10 of " + ident;
                case 11:
                    return "Jack of " + ident;
                case 12:
                    return "Queen of " + ident;
                case 13:
                    return "King of " + ident;
                case 14:
                    return "Ace of " + ident;
            }

            return ident;

        }

        public static void Shuffle (Dictionary<int, string> myDictionary)
        {
            Random rand = new Random();
            myDictionary = myDictionary.OrderBy(x => rand.Next())
                .ToDictionary(item => item.Key, item => item.Value);

            int l = 1;

            foreach (KeyValuePair<int, string> kvp in myDictionary)
            {
                Console.WriteLine(l + "   " + kvp.Value);
                l++;
            }
            

        }

        public static void Shop ()
        {
            Queue<string> shopQueue = new Queue<string>();

            shopQueue.Enqueue("Jalmari");
            shopQueue.Enqueue("Anni");
            shopQueue.Enqueue("Kai");
            shopQueue.Enqueue("Pekka");
            shopQueue.Enqueue("Heli");

            int numQueue = shopQueue.Count;

            for (int i = 0; i < numQueue; i++)
            {
                Console.WriteLine("Jonossa on yhteensä " + shopQueue.Count + " henkilöä");
                Console.WriteLine("Kassan jonossa seuraavaksi on: " + shopQueue.Peek());
                Console.WriteLine("");
                Console.WriteLine("Paina jotain näppäintä niin " + shopQueue.Peek() + " pääse kassalle!");
                Console.ReadKey();
                Console.Clear();
                shopQueue.Dequeue();
            }

            Console.WriteLine("Jonossa ei ole enä asiakkaita!");
            Console.WriteLine("Paina jotain näppäintä niin alkaa elokuva ohjelma");
            Console.ReadKey();
            Console.Clear();

        }


        public static void Movies()
        {
            List<string> movieList = new List<string>();
            bool check = false;

            Stopwatch sw = Stopwatch.StartNew();
            do
            {

                do
                {
                    StringBuilder movie = new StringBuilder();

                    for (int i = 0; i < GetNum(); i++)
                    {
                        movie.Insert(0, Convert.ToString(GetLetter()));
                    }

                    movie.Insert(0, " ");
                    movie.Insert(0, Convert.ToString(GetYear()));
                    movie.Insert(0, " ");

                    for (int i = 0; i < GetNum(); i++)
                    {
                        movie.Insert(0, Convert.ToString(GetLetter()));
                    }

                    if (movieList.Contains(movie.ToString()))
                    {
                        check = false;
                    }
                    else
                    {
                        movieList.Add(movie.ToString());
                        check = true;
                    }

                } while (check == false);

            } while (movieList.Count < 10000);

            Console.WriteLine(sw.ElapsedMilliseconds + " ms (List) 10 000 elkouva");
            Console.WriteLine("Elokuvat ovat List:assa string:eina muodossa nimi vuosi ohjaaja");
            sw.Stop();

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin etsin sinulle listasta randomilla");
            Console.WriteLine("1000 elokuva, missä nimi alkaa vokaalilla (a,e,i,o tai u)");
            Console.WriteLine("Lisäksi mittaan kuinka kauan tämä toiminta kestää");
            Console.ReadKey();

            sw.Start();
            List<string> movieNames = new List<string>();

            foreach (string element in movieList)
            {
                if (element.StartsWith("a") ^ element.StartsWith("e") ^ element.StartsWith("i") ^ element.StartsWith("o") ^ element.StartsWith("u"))
                {
                    movieNames.Add(element);
                }
            }

            var shuffled = movieNames.OrderBy(x => Guid.NewGuid()).ToList();

            for (int k = 0; k<1000; k++)
            {
                Console.WriteLine((k+1) + " " + shuffled[k]);
            }

            Console.WriteLine("");
            Console.WriteLine(sw.ElapsedMilliseconds + " ms (1000 elokuva, random, haku + tulostus)");
            sw.Stop();
        }

        
        public static void MoviesDict ()
        {
            Dictionary<string, string> moviesInD = new Dictionary<string, string>();

            bool check = false;

            Stopwatch sw = Stopwatch.StartNew();
            do
            {

                do
                {
                    StringBuilder name = new StringBuilder();

                    for (int i = 0; i < GetNum(); i++)
                    {
                        name.Insert(0, Convert.ToString(GetLetter()));
                    }

                    StringBuilder yearPlus = new StringBuilder();

                    yearPlus.Insert(0, Convert.ToString(GetYear()));
                    yearPlus.Insert(0, " ");

                    for (int i = 0; i < GetNum(); i++)
                    {
                        yearPlus.Insert(0, Convert.ToString(GetLetter()));
                    }

                    if (moviesInD.ContainsKey(Convert.ToString(name)))
                    {
                        check = false;
                    }
                    else
                    {
                        moviesInD.Add(Convert.ToString(name), Convert.ToString(yearPlus));
                        check = true;
                    }

                } while (check == false);

            } while (moviesInD.Count < 10000);

            Console.WriteLine(sw.ElapsedMilliseconds + " ms (Dictionary) 10 000 elkouva");
            Console.WriteLine("Elokuvat ovat Dictionary:ssa muodossa string:string");
            Console.WriteLine("nimi:vuosi ohjaaja");
            sw.Stop();

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin etsin sinulle Dictionarysta randomilla");
            Console.WriteLine("1000 elokuva, missä nimi alkaa vokaalilla (a,e,i,o tai u)");
            Console.WriteLine("Lisäksi mittaan kuinka kauan tämä toiminta kestää");
            Console.ReadKey();

            sw.Start();

            Dictionary<string, string> movieSearch = new Dictionary<string, string>();

            foreach (KeyValuePair<string, string> kvp in moviesInD)
            {
                if (kvp.Key.StartsWith("a") ^ kvp.Key.StartsWith("e") ^ kvp.Key.StartsWith("i") ^ kvp.Key.StartsWith("o") ^ kvp.Key.StartsWith("u"))
                {
                    movieSearch.Add(kvp.Key, kvp.Value);
                }
            }

            Random rand = new Random();
            movieSearch = movieSearch.OrderBy(x => rand.Next())
                .ToDictionary(item => item.Key, item => item.Value);

            int h = 1;

            foreach (KeyValuePair<string, string> kvp in movieSearch)
            {
                h++;
                Console.WriteLine(h + " " + kvp.Key + " " + kvp.Value);
                if (h > 999)
                {
                    break;
                }
            }
            

            Console.WriteLine("");
            Console.WriteLine(sw.ElapsedMilliseconds + " ms (1000 elokuva, random, haku + tulostus)");
            sw.Stop();
        }




        public static Random _random = new Random();

        public static char GetLetter()
        {
            int num = _random.Next(0, 26);
            char let = (char)('a' + num);
            return let;
        }

        public static int GetNum()
        {
            int num = _random.Next(4, 11);
            return num;
        }

        public static int GetYear()
        {
            int num = _random.Next(1920, 2020);
            return num;
        }

    }
}
