﻿using System;

namespace Harj07
{
    class Program
    {
        static void Main()
        {
            Age();
            Console.Clear();
            HighestNumber();
            Console.Clear();
            NumberCheck();
            Console.Clear();
            Grade();
            Console.Clear();
            SearchingZero();
            Console.Clear();
            LeapYear();
            Console.WriteLine("Ohjelma loppui, Kiitos!");
            Console.ReadKey();
        }

        public static void Age()
        {
            Console.WriteLine("Tämä ohjelma kysy sinulta iän ja tulosta vastauksena iäluokan");
            Console.WriteLine("Mikä on ikäsi?");
            string ageString = Console.ReadLine();
            short ageNum = Convert.ToInt16(ageString);

            if (ageNum > 0 & ageNum < 13)
            {
                Console.WriteLine("");
                Console.WriteLine("child");
            }
            else if (ageNum > 12 & ageNum < 20)
            {
                Console.WriteLine("");
                Console.WriteLine("teen");
            }
            else if (ageNum > 19 & ageNum < 66)
            {
                Console.WriteLine("");
                Console.WriteLine("adult");
            }
            else
            {
                Console.WriteLine("");
                Console.WriteLine("senior");
            }
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä ja seuraava ohjelma alkaa");
            Console.ReadKey();
        }

        public static void HighestNumber()
        {
            Console.WriteLine("tämä ohjelma kysy sinulta kolme numeroa ja tulosta niistä suurimman");
            Console.WriteLine("");
            Console.WriteLine("Kirjoita ensimmäinen numero");
            string num1Str = Console.ReadLine();
            int num1 = Convert.ToInt16(num1Str);
            Console.WriteLine("");
            Console.WriteLine("Kirjoita toinen numero");
            string num2Str = Console.ReadLine();
            int num2 = Convert.ToInt16(num2Str);
            Console.WriteLine("");
            Console.WriteLine("Kirjoita kolmas numero");
            string num3Str = Console.ReadLine();
            int num3 = Convert.ToInt16(num3Str);

            if (num1 > num2 & num1 > num3)
            {
                Console.WriteLine("Suurin numero on: " + num1);
            }
            else if (num2 > num1 & num2 > num3)
            {
                Console.WriteLine("Suurin numero on: " + num2);
            }
            else if (num3 > num1 & num3 > num2)
            {
                Console.WriteLine("Suurin numero on: " + num3);
            }
            else
            {
                Console.WriteLine("Suurinta numeroa ei ole");
            }
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä ja seuraava ohjelma alkaa");
            Console.ReadKey();
        }

        public static void NumberCheck()
        {
            Console.WriteLine("Tämä ohjelma kysy sinulta numeron (kokonainen) ja vertaile sitä");
            Console.WriteLine("onko numeron on 10 tai 20 , 100 tai 200, tai joku muu ");
            Console.WriteLine("");
            Console.WriteLine("Kirjoita numero: ");
            string number = Console.ReadLine();
            int num = Convert.ToInt32(number);
            Console.WriteLine("");

            switch (num)
            {
                case 10:
                    Console.WriteLine("Number is 10 or 20");
                    break;
                case 20:
                    Console.WriteLine("Number is 10 or 20");
                    break;
                case 100:
                    Console.WriteLine("Number is 100 or 200");
                    break;
                case 200:
                    Console.WriteLine("Number is 100 or 200");
                    break;
                default:
                    Console.WriteLine("Number is " + num);
                    break;
            }

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä ja seuraava ohjelma alkaa");
            Console.ReadKey();
        }

        public static void Grade()
        {
            Console.WriteLine("Kuinka paljon sait pisteitä kokesta? (0-12)");
            string points = Console.ReadLine();
            short grade = Convert.ToInt16(points);

            if (grade == 0 ^ grade == 1)
            {
                Console.WriteLine("Arvosanasi on: " + 0);
            }
            else if (grade == 2 ^ grade == 3)
            {
                Console.WriteLine("Arvosanasi on: " + 1);
            }
            else if (grade == 4 ^ grade == 5)
            {
                Console.WriteLine("Arvosanasi on: " + 2);
            }
            else if (grade == 6 ^ grade == 7)
            {
                Console.WriteLine("Arvosanasi on: " + 3);
            }
            else if (grade == 8 ^ grade == 9)
            {
                Console.WriteLine("Arvosanasi on: " + 4);
            }
            else
            {
                Console.WriteLine("Arvosanasi on: " + 5);
            }

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä ja seuraava ohjelma alkaa");
            Console.ReadKey();

        }

        private static void SearchingZero()
        {
            int total = 0;
            int num;
            do
            {
                Console.Clear();
                Console.WriteLine("Kirjoita numero, jos numero on mitään muuta kun 0, kysyn sinulta uuden");
                Console.WriteLine("numeron. Jos numero on 0, tulosta sinulle kaikkien kirjoittamasi numeroiden summan");
                Console.WriteLine("Sisäistä numero: ");
                string number = Console.ReadLine();
                num = Convert.ToInt16(number);
                if (num != 0)
                {
                    total += num;
                } else
                {
                    Console.WriteLine("Kaikkien numeroiden summa on: " + total);
                }
            } while (num != 0);

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä ja seuraava ohjelma alkaa");
            Console.ReadKey();
        }

        public static void LeapYear()
        {
            Console.WriteLine("Kirjota vuosinumero, kerron sinulle onko se karkausvuosi");
            Console.WriteLine("Sisäistä vuosi: ");
            string year = Console.ReadLine();
            int yearNum = Convert.ToInt16(year);
            if ((yearNum % 400 == 0) ^ ((yearNum % 4 == 0) && (yearNum % 100 != 0)))
            {
                Console.WriteLine("Sisäistämäsi vuosi ON karkausvuosi");
            }
            else
            {
                Console.WriteLine("Sisäistämäsi vuosi EI OLE karkausvuosi");
            }
        }
    }
}
