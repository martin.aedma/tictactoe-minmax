let currentBoard = [0,1,2,3,"X",5,6,7,8];

let human = "X";

let computer = "O";

function isWinning (board, player){
    if (
        (board[0] == player && board [1] == player && board [2] == player) ||
        (board[3] == player && board [4] == player && board [5] == player) ||
        (board[6] == player && board [7] == player && board [8] == player) ||
        (board[0] == player && board [3] == player && board [6] == player) ||
        (board[1] == player && board [4] == player && board [7] == player) ||
        (board[2] == player && board [5] == player && board [8] == player) ||
        (board[0] == player && board [4] == player && board [8] == player) ||
        (board[2] == player && board [4] == player && board [6] == player)
    ) {return true;} 
    else {return false;}
}

function availableSquares (board) {
    return board.filter(square => square != "O" && square != "X");
}

let bestSquare = minMax (currentBoard, computer);

function minMax (board, player) {

    let freeSquares = availableSquares(board);

    if (isWinning(board, human)) {
        return {score:-10};
    } else if (isWinning(board, computer)) {
        return {score:10};
    } else if (freeSquares.length === 0) {
        return {score:0};
    }

    const moves = [];

    for (let i=0; i < freeSquares.length; i++) {
        const move = {};
        move.index = board[freeSquares[i]];

        board[freeSquares[i]] = player;

        if (player == computer) {
            let result = minMax (board, human);
            move.score = result.score;
        } else {
            let result = minMax (board, computer);
            move.score = result.score;
        }

        board[freeSquares[i]] = move.index;

        moves.push(move);
    }

    let bestMove;
    if(player === computer) {
        let bestScore = -10000;
        for (let i=0; i < moves.length; i++) {
            if (moves[i].score > bestScore) {
                bestScore = moves[i].score;
                bestMove = i;
            }
        }
    } else {
        let bestScore = 10000;
        for (let i=0; i<moves.length; i++) {
            if (moves[i].score < bestScore) {
                bestScore = moves[i].score;
                bestMove = i;
            }
        }
    }

    return moves[bestMove];
}

console.log(bestSquare);
