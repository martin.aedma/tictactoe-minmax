﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Dynamic;


namespace Experiment
{
    using static Globals;

    class Program
    {
        static void Main()
        {
            int result = Algorithms.MinMax(board, computer);
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }

    


    class Algorithms
    {
        public static bool IsWinning(int[] board, int player)
        {
            if (
        (board[0] == player && board[1] == player && board[2] == player) |
        (board[3] == player && board[4] == player && board[5] == player) |
        (board[6] == player && board[7] == player && board[8] == player) |
        (board[0] == player && board[3] == player && board[6] == player) |
        (board[1] == player && board[4] == player && board[7] == player) |
        (board[2] == player && board[5] == player && board[8] == player) |
        (board[0] == player && board[4] == player && board[8] == player) |
        (board[2] == player && board[4] == player && board[6] == player)
                )
            { return true; }
            else { return false; }
        }

        public static int[] AvailableSquares(int[] currentBoard)
        {
            int[] freesquares = Array.FindAll(currentBoard, c => c != 11 && c != 22);
            return freesquares;
        }

        public static int MinMax(int[] board, int player)
        {
            int[] freeSquares = AvailableSquares(board);

            if (IsWinning(board, computer))
            {
                int min = 10;
                return min;
            }
            else if (IsWinning(board, human))
            {
                int max = -10;
                return max;
            }
            else if (freeSquares.Length == 0)
            {
                int draw = 0;
                return draw;
            }

            var moves = new List<dynamic>();

            for (int i = 0; i < freeSquares.Length; i++)
            {
                dynamic move = new ExpandoObject();
                move.index = freeSquares[i];

                board[freeSquares[i]] = player;

                if (player == computer)
                {
                    int result = MinMax(board, human);
                    move.score = result;
                }
                else
                {
                    int result = MinMax(board, computer);
                    move.score = result;
                }

                board[freeSquares[i]] = move.index;
                moves.Add(move);

            }

            int bestMove = 0;

            if (player == computer)
            {
                int bestScore = -10000;
                for (int i = 0; i < moves.Count; i++)
                {
                    if (moves[i].score > bestScore)
                    {
                        bestScore = moves[i].score;
                        bestMove = i;
                    }
                }
            }
            else
            {
                int bestScore = 10000;
                for (int i = 0; i < moves.Count; i++)
                {
                    if (moves[i].score < bestScore)
                    {
                        bestScore = moves[i].score;
                        bestMove = i;
                    }
                }
            }

            return moves[bestMove].index;

        }

    }
}
