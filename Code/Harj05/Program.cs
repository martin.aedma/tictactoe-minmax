﻿using System;

namespace Harj05
{
    class Program
    {

        static void Main(string[] args)
        {
            bool state = true;
            do
            {
                welcomeFunction();
                measureLength();
                addTwoNumbers();
                calculation();
                if (banking() == true)
                {
                    state = true;
                } else
                {
                    state = false;
                }

            } while (state == true);
        }

        private static void welcomeFunction()
        {
            Console.Clear();
            Console.WriteLine("Kirjoita nimesi: ");
            string name = Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine("Here starts basics of programming with C#");
            Console.WriteLine(name);
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin alkaa seuraava ohjelma");
            Console.ReadKey();
            return;
        }

        private static void measureLength()
        {
            Console.Clear();
            Console.WriteLine("Kirjoita pituuteesi sentimettreissä: ");
            Console.WriteLine("");
            string length = Console.ReadLine();
            short lengthNumber = Convert.ToInt16(length);
            Console.WriteLine("");
            Console.WriteLine("Sinun pituuteesi on " + lengthNumber + " cm");
            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin alkaa seuraava ohjelma");
            Console.ReadKey();
            return;
        }

        private static void addTwoNumbers()
        {
            // valitsin long muutujan jotta olisi myös mahdollista laitta alyttöman suuria numeroita
            Console.Clear();
            Console.WriteLine("Tämä ohjelma tulosta kahden kokonaisluvun summan");
            Console.WriteLine("");
            Console.WriteLine("Kirjoita ensimmäinen kokonainen luku: ");
            string first = Console.ReadLine();
            long number1 = Convert.ToInt64(first);
            Console.WriteLine("");
            Console.WriteLine("Kirjoita toinen kokonainen luku: ");
            string second = Console.ReadLine();
            long number2 = Convert.ToInt64(second);
            Console.WriteLine("");
            long number3 = number1 + number2;
            Console.WriteLine("Niiden numeroiden summa on: " + number3);
            Console.WriteLine("Paina jotain näppäintä niin alkaa seuraava ohjelma");
            Console.ReadKey();
            return;
        }

        private static void calculation()
        {
            // Valitsen taas long muuttujan jotta olisi mahdollista laitta myös tosi isoja numeroita
            // jakossa otaan käyttöön decimal muutujan jotta tuloksena voisi olla myös decimaali arvo

            Console.Clear();
            Console.WriteLine("Tämä ohjelma tulosta kahden kokonaisluvun: summan / eron / tulon / jakon");
            Console.WriteLine("");
            Console.WriteLine("Kirjoita ensimmäinen kokonainen luku: ");
            string first = Console.ReadLine();
            long number1 = Convert.ToInt64(first);
            Console.WriteLine("");
            Console.WriteLine("Kirjoita toinen kokonainen luku: ");
            string second = Console.ReadLine();
            long number2 = Convert.ToInt64(second);
            Console.WriteLine("");
            long add = number1 + number2;
            long subtract = number1 - number2;
            long multiply = number1 * number2;
            decimal divide = decimal.Divide(number1, number2);
            Console.WriteLine("");
            Console.WriteLine("Antamasi numeroiden laskelmat ovat: ");
            Console.WriteLine("Summa: " + add);
            Console.WriteLine("Ero (num1-num2: " + subtract);
            Console.WriteLine("Tulo: " + multiply);
            Console.WriteLine("Jako (num1/num2): " + divide);
            Console.WriteLine("Paina jotain näppäintä niin alkaa seuraava ohjelma");
            Console.ReadKey();
            return;
        }

        private static bool banking()
        {
            Console.Clear();
            Console.WriteLine("Kirjota pankkitilisi saldo euroissa: ");
            string accountBalance = Console.ReadLine();
            decimal balance = Convert.ToDecimal(accountBalance);
            Console.WriteLine("");
            Console.WriteLine("Montako euroa haluat lisätä? : ");
            string addEuro = Console.ReadLine();
            int euros = Convert.ToInt32(addEuro);
            Console.WriteLine("");
            Console.WriteLine("Montako senttiä haluat lisätä? : ");
            string addCents = Console.ReadLine();
            decimal cents = (Convert.ToDecimal(addCents)) / 100;
            decimal final = balance + euros + cents;
            Console.WriteLine("");
            Console.WriteLine("Lisäyksien jälkeen lopullinen saldosi on: " + final);
            Console.WriteLine("");
            Console.WriteLine("Paina ENTER jos haluat aloittaa koko ohjelman uudelleen. Paina jotain muuta jos haluat lopetta ohjelman");
            ConsoleKeyInfo info = Console.ReadKey();
            if (info.Key == ConsoleKey.Enter)
            {
                return true;
            } else 
            { 
                return false;
            }
            
            
        }
    }
}
