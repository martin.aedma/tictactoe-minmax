﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Dynamic;


namespace Harjotustyö
{
    using static Globals;

    /* Algorithms luokka sisältää metodeja mitkä ovat olennaisa pelin toimintaan, erityisesti kun kone tekee siirron */

    class Algorithms 
    {
        // Voittotilanteen tarkaistus, 8 mahdollista variaatiota    
        public static bool IsWinning(int[] board, int player)
        {
            if (
        (board[0] == player && board[1] == player && board[2] == player) ||
        (board[3] == player && board[4] == player && board[5] == player) ||
        (board[6] == player && board[7] == player && board[8] == player) ||
        (board[0] == player && board[3] == player && board[6] == player) ||
        (board[1] == player && board[4] == player && board[7] == player) ||
        (board[2] == player && board[5] == player && board[8] == player) ||
        (board[0] == player && board[4] == player && board[8] == player) ||
        (board[2] == player && board[4] == player && board[6] == player)
                )
            { return true; }
            else { return false; }
        }

        // Luotaan uusi taulukko missä ovat pelipöydän vapaat ruutut
        public static int[] AvailableSquares(int[] currentBoard)
        {
           int[] freesquares = Array.FindAll(currentBoard, c => c !=11  && c !=22);
            return freesquares;
        }

        // Konen siirto tulee tämän rekursiivisen funktion toiminnasta
        public static object MinMax(int[] board, int player)
        {
            int[] freeSquares = AvailableSquares(board);

            //rekursiivisen funktion loppu: jompikumpi pelaaja voitta tai tasapeli. Kone max(+) ihminen min(-)
            if (IsWinning(board, computer))
            {
                Move max = new Move();
                max.score = 10;
                return max;
            }
            else if (IsWinning(board, human))
            {
                Move min = new Move();
                min.score = -10;
                return min;
            }
            else if (freeSquares.Length == 0)
            {
                Move draw = new Move();
                draw.score = 0;
                return draw;
            }

            //Kerätään kaikki mahdolliset siirrot ja niiden score
            var moves = new List<dynamic>();

            //Käy läpi kaikki vapaana olevat ruutut
            for (int i = 0; i < freeSquares.Length; i++)
            {
                //luotaan uusi Move luokan objektti jokaista siirota varten missä on siirron indexi ja sen arvo score
                Move move = new Move();
                move.index = freeSquares[i];
                board[freeSquares[i]] = player;

                /* Tässä tapahtuu rekursio, jos on konen vuoro niin uusi MinMax kutsutaan pelaajan "vuorolla"
                 ja toisinpäin myös */

                if (player == computer)
                {

                    Move result = new Move();
                    result = (Move)MinMax(board, human);
                    move.score = result.score;
                }
                else
                {
                    Move result = new Move();
                    result = (Move)MinMax(board, computer);
                    move.score = result.score;
                }

                // vapaudedaan käsitelyty ruutu ja lisätään siirto (move) moves listaan
                board[freeSquares[i]] = move.index;
                moves.Add(move);

            }

            /* kun kaikki mahdolliset siirrot yhdltä tasolta on kerätty, etsitään niistä paras mahdollinen 
            sille pelaajalle kene vuoro on - kone maximalisoi pelaaja minimalisoi*/
            int bestMove = 0;

            if (player == computer)
            {
                int bestScore = -10000;
                for (int i = 0; i < moves.Count; i++)
                {
                    if (moves[i].score > bestScore)
                    {
                        bestScore = moves[i].score;
                        bestMove = i;
                    }
                }
            }
            else
            {
                int bestScore = 10000;
                for (int i = 0; i < moves.Count; i++)
                {
                    if (moves[i].score < bestScore)
                    {
                        bestScore = moves[i].score;
                        bestMove = i;
                    }
                }
            }

            //tason paras siirto löydetty, palautetaan se ylöspäin edelliselle pelaajalle
            return moves[bestMove];

        }

    }
}