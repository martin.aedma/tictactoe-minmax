﻿using System;


namespace Harjotustyö
{

    using static Globals;


    //Pääohjelma, kutsutaan eri luokkien metodeja pelin pelaamiseen ja toimimiseen
    class Program
    {
        static void Main()
        {
            bool game = true;

            do
            {
                //valitaan puoli tai lopetetaan peli
                if (Choice() == true)
                {
                    bool currentGame = true;
                    //jos peltaan sitten niin kauan kun voitto tai tasapeli
                    do
                    {
                        //kun kone on X ja tekee ensimmäisen siirron sit heitta keskeelle, ei tarvii laskea. 
                        if (human == 22 && Algorithms.AvailableSquares(board).Length == 9)
                        {
                            board[4] = computer;
                        } 
                        
                        //muuten pelaaja aloittaa
                        else
                        {
                            //Pelaaja tekee siirön
                            Structure.DrawBoard();
                            Structure.MakeMove(board);

                            //tuliko siirron seuraksena tasapeli
                            if (Algorithms.AvailableSquares(board).Length == 0)
                            {
                                Structure.Draw();
                                currentGame = false;
                                Structure.Log("Ristinolla log.txt", Structure.Convert(board));
                            } 
                            //tai voitto (mahdoton jos kone tekee parhaan siirron)
                            else if (Algorithms.IsWinning(board, human))
                            {
                                Structure.HumanWon();
                                currentGame = false;
                                Structure.Log("Ristinolla log.txt", Structure.Convert(board));
                            }
                            
                            //jos edellä peli ei päättynyt niin konen vuoro
                            else
                            {

                                Move AiMove = new Move();
                                AiMove = (Move)Algorithms.MinMax(board, computer);
                                
                                board[AiMove.index] = computer;

                                //voittiko kone
                                if (Algorithms.IsWinning(board, computer))
                                {
                                    Structure.CompWon();
                                    currentGame = false;
                                    Structure.Log("Ristinolla log.txt", Structure.Convert(board));
                                }
                                // tai tasapeli
                                else if (Algorithms.AvailableSquares(board).Length == 0)
                                {
                                    Structure.Draw();
                                    currentGame = false;
                                    Structure.Log("Ristinolla log.txt", Structure.Convert(board));
                                }
                            }

                        }

                        // jos peli ei päättynyt niin tehdään uusi siirto kunnes peli päätty
                    } while (currentGame == true);

                } 

                //jos pelaaja valitse ennen uutta pelia ESCape niin ohjelma loppuu
                else

                {
                    game = false;
                }

            } while (game == true);
             
        }

        
    }
}
