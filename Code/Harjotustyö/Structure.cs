﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;

namespace Harjotustyö
{
    using static Globals;

    //Structure luokka sisältää metodeja pelin "runkon" tekemiseksi. Pöytä ja muut viestit, ASCII arti

    class Structure
    {
        
        public static void DrawBoard()
        {
            Console.Clear();
            
            Console.WriteLine(@"                     ____  ___ ____ _____ ___ _   _  ___  _     _        _    ");
            Console.WriteLine(@"                    |  _ \|_ _/ ___|_   _|_ _| \ | |/ _ \| |   | |      / \   ");
            Console.WriteLine(@"                    | |_) || |\___ \ | |  | ||  \| | | | | |   | |     / _ \  ");
            Console.WriteLine(@"                    |  _ < | | ___) || |  | || |\  | |_| | |___| |___ / ___ \ ");
            Console.WriteLine(@"                    |_| \_\___|____/ |_| |___|_| \_|\___/|_____|_____/_/   \_\");


            VisualBoard();

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("                                   Sinun vuoro:");
            Console.WriteLine("                                   Valitse VAPAA ruutu");
            Console.WriteLine("                                   Paina numeroa 0-8!");

        }

        // Pelaajan siirto
        public static void MakeMove (int[] board)
        {
            bool control = false;

            do
            {
                int number = -1;
                ConsoleKeyInfo info = Console.ReadKey(true);
                if (char.IsDigit(info.KeyChar))
                {
                    number = int.Parse(info.KeyChar.ToString());
                }
                else
                {
                    number = -1;
                }

                if (number >= 0 && number < 9 && Array.Exists(board, element => element == number))
                {
                    control = true;
                    board[number] = human;
                }
                else
                { control = false; }
            } while (control == false);
                    
        }

        public static void CompWon ()
        {
            Console.Clear();
                                                
              Console.WriteLine(@"                           _  _____  _   _ _____  __     _____ ___ _____ _____ ___ _ ");
              Console.WriteLine(@"                          | |/ / _ \| \ | | ____| \ \   / / _ |_ _|_   _|_   _|_ _| |");
              Console.WriteLine(@"                          | ' | | | |  \| |  _|    \ \ / | | | | |  | |   | |  | || |");
              Console.WriteLine(@"                          | . | |_| | |\  | |___    \ V /| |_| | |  | |   | |  | ||_|");
              Console.WriteLine(@"                          |_|\_\___/|_| \_|_____|    \_/  \___|___| |_|   |_| |___(_)");

            VisualBoard();

        }
        
        
        public static void Draw ()
        {
            Console.Clear();
            
            Console.WriteLine(@"                           _____  _    ____    _    ____  _____ _     ___ ");
              Console.WriteLine(@"                        |_   _|/ \  / ___|  / \  |  _ \| ____| |   |_ _|");
              Console.WriteLine(@"                          | | / _ \ \___ \ / _ \ | |_) |  _| | |    | | ");
              Console.WriteLine(@"                          | |/ ___ \ ___) / ___ \|  __/| |___| |___ | | ");
              Console.WriteLine(@"                          |_/_/   \_|____/_/   \_|_|   |_____|_____|___|");

            VisualBoard();

        }

        //Teoriassa ja käytännössä tämä on vähän vitsi kun ei pitäisi ikinä tapahtua :)
        public static void HumanWon()
        {
            Console.Clear();
            Console.WriteLine(@"                           ____ ___ _   _ _   _ _   _  __     _____ ___ _____ _____ ___  _ ");
            Console.WriteLine(@"                          / ___|_ _| \ | | | | | \ | | \ \   / / _ |_ _|_   _|_   _/ _ \| |");
            Console.WriteLine(@"                          \___ \| ||  \| | | | |  \| |  \ \ / | | | | |  | |   | || | | | |");
            Console.WriteLine(@"                           ___) | || |\  | |_| | |\  |   \ V /| |_| | |  | |   | || |_| |_|");
            Console.WriteLine(@"                          |____|___|_| \_|\___/|_| \_|    \_/  \___|___| |_|   |_| \___/(_)");
            Console.WriteLine("");

            VisualBoard();
 
        }

        //Pelin loppu asetuksen tallennus tiedotsoon, kuka voitti / tasapeli ja päiväys
        public static void Log (string text, string[] array)
        {
            //yritetään tallentaa
            try
            {
                string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                path = Path.Combine(path, text);

                string gameLog = string.Join(",", array.Select(p => p.ToString()).ToArray());
                DateTime now = DateTime.Now;
                string asString = now.ToString("dd MMMM yyyy hh:mm:ss tt");

                string status;

                if (Algorithms.IsWinning(board, computer))
                {
                    status = "Kone voitti";
                }
                else if (Algorithms.IsWinning(board, human))
                {
                    status = "Player voitti";
                }
                else
                {
                    status = "Tasapeli";
                }

                File.AppendAllText(path, gameLog + Environment.NewLine + status + Environment.NewLine + asString + Environment.NewLine + Environment.NewLine, Encoding.UTF8);
                Console.WriteLine("Pelin loppuasetelma tallenettu MyDocuments/Ristinolla log.txt");
                Console.WriteLine("Paina jotain näppäintä ... ");
            } 
            //jos ei onnistuu niin ohjelma jatkuu ilmoittamalla että ei onnistunut
            catch
            {
                Console.WriteLine("Yritin tallentaa pelin MyDocuments/Ristinolla log.txt mutta ei onnistunut");
                Console.WriteLine("Paina jotain näppäintä ... ");
            }
            //pöytä nollataan uutta pelia varten
            for (int i = 0; i < 9; i++)
            {
                board[i] = i;
            }

            Console.ReadKey();
        }

        //Pöytän taulukko käännetään int > string jotta voitais tulsota X ja O merkkeja
        public static string[] Convert (int[] array)
        {
            string[] display = array.Select(x => x.ToString()).ToArray();

            for (int i = 0; i < 9; i++)
            {
                if (display[i] == "11")
                {
                    display[i] = "X";
                }
                else if (display[i] == "22")
                {
                    display[i] = "O";
                }
            }

            return display;
        }

        public static void VisualBoard ()
        {
            string[] display = Convert(board);

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("                                   -------------------------");
            Console.WriteLine("                                   |       |       |       |");
            Console.WriteLine("                                   |   {0}   |   {1}   |   {2}   |", display[0], display[1], display[2]);
            Console.WriteLine("                                   |       |       |       |");
            Console.WriteLine("                                    -------------------------");
            Console.WriteLine("                                   |       |       |       |");
            Console.WriteLine("                                   |   {0}   |   {1}   |   {2}   |", display[3], display[4], display[5]);
            Console.WriteLine("                                   |       |       |       |");
            Console.WriteLine("                                   -------------------------");
            Console.WriteLine("                                   |       |       |       |");
            Console.WriteLine("                                   |   {0}   |   {1}   |   {2}   |", display[6], display[7], display[8]);
            Console.WriteLine("                                   |       |       |       |");
            Console.WriteLine("                                   -------------------------");
        }

    }
}
