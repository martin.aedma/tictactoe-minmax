﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Harjotustyö
{
    public static class Globals
        // Globals käsittele muutama yleisesti käytettyä muuttuja sekä metodia
    {
        public static int human;
        public static int computer;
        public static int[] board = {0,1,2,3,4,5,6,7,8};
        
        //valitaan puoli tai lopetetaan peli
        public static bool Choice()
        {
            bool game = true;
            bool check = true;
            
            Console.Clear();

            Console.WriteLine(@"                     ____  ___ ____ _____ ___ _   _  ___  _     _        _    ");
            Console.WriteLine(@"                    |  _ \|_ _/ ___|_   _|_ _| \ | |/ _ \| |   | |      / \   ");
            Console.WriteLine(@"                    | |_) || |\___ \ | |  | ||  \| | | | | |   | |     / _ \  ");
            Console.WriteLine(@"                    |  _ < | | ___) || |  | || |\  | |_| | |___| |___ / ___ \ ");
            Console.WriteLine(@"                    |_| \_\___|____/ |_| |___|_| \_|\___/|_____|_____/_/   \_\");

            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("                      X tai O");
            Console.WriteLine("                      X aloittaa");
            Console.WriteLine("");
            Console.WriteLine("                      Valitse X painamalla x");
            Console.WriteLine("                      Valitse O painamalla o");
            Console.WriteLine("");
            Console.WriteLine("                      Painamalla ESCape peli loppuu");
            Console.WriteLine("");
            Console.WriteLine("                      Valitse: ");

            do
            {
            
            ConsoleKeyInfo info = Console.ReadKey(true);

                if (info.Key == ConsoleKey.X)
                {
                    human = 11;
                    computer = 22;
                    check = false;
                    return game;

                }
                else if (info.Key == ConsoleKey.O)
                {
                    human = 22;
                    computer = 11;
                    check = false;
                    return game;

                }
                else if (info.Key == ConsoleKey.Escape)
                {
                    game = false;
                    return game;
                }
                

            } while (check == true);

            return game;

        }

    }

    // Luokka MinMax algoritmin siirtoja varten, jokaisella mahdollisella siirrolla on mukaana Index ja Score 
    public class Move
    {
        public int index;
        public int score;
    }
    
}
