﻿using System;

namespace Harj12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kuinka monta kokonaislukua luodaan? Sisäistä numero: ");
            string number = Console.ReadLine();
            int num = Convert.ToInt16(number);
            Console.WriteLine("");

            int[] array = new int[num];

            for (int i=0; i < num; i++)
            {
                array[i] = i + 10;
                
                Console.WriteLine(array[i]);
            }

            Console.WriteLine("");

            for (int a = num; a > 0; a--)
            {
                Console.WriteLine(array[a-1]);
            }

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin ohjelma jatkuu: ");
            Console.WriteLine("");
            Console.ReadKey();

            int[] numbers = { 1, 2, 33, 44, 55, 68, 77, 96, 100 };

            foreach (int value in numbers)
            {
                if (value % 2 == 0)
                {
                    Console.WriteLine("EVEN " + value);
                }
            }

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin ohjelma jatkuu: ");
            Console.ReadKey();

            int[] points = new int[5];

            for (int k = 0; k < 5; k++)
            {
                Console.Clear();
                Console.WriteLine("Sisäistä " + (k + 1) + ". tyylipisteet");
                string point = Console.ReadLine();
                points[k] = Convert.ToInt16(point);
            }

            int minint = points[0];
            int maxint = points[0];
            foreach (int value in points)
            {
                if (value < minint) minint = value;
                if (value > maxint) maxint = value;
            }

            Console.WriteLine("");
            int sum = 0;
            foreach (int l in points) sum += l;
            sum = sum - maxint - minint;


            decimal average = decimal.Divide(sum, 3);

            if (sum % 3 != 0)
            {
                Console.WriteLine("Keskiarvo, Rounded: " + Math.Round(average));
            }

            Console.WriteLine("keskiarvo (decimaaleilla jos niitä on): " + average);

            Console.WriteLine("");
            Console.WriteLine("Paina jotain näppäintä niin ohjelma jatkuu: ");
            Console.ReadKey();
            Console.Clear();

            Random rnd = new Random();

            int[] rndnum = new int[10];

            Console.WriteLine("Kymmenen random numero: ");

            for (int h=0; h<10; h++)
            {
                rndnum[h] = rnd.Next(1, 101);
                Console.WriteLine(rndnum[h]);
            }

            int e, r, temp;

            for (e=0; e<10; e++)
            {
                for(r=e+1; r<10; r++)
                {
                    if (rndnum[e] < rndnum[r])
                    {
                        temp = rndnum[e];
                        rndnum[e] = rndnum[r];
                        rndnum[r] = temp;
                    }
                }
            }

            Console.WriteLine("");
            Console.WriteLine("Nämä numerot alkaen isommasta: ");

            for (int t = 0; t<10; t++)
            {
                Console.WriteLine(rndnum[t]);
            }

            Console.ReadKey();

            

            

        }
    }
}
