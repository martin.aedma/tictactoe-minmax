﻿using System;

namespace Harj10
{


    public struct Vehicle
    {
       public string name;
       public int modelYear;
       public byte efficiency;

        public override string ToString()
        {
            return name + ", " + modelYear + ", " + efficiency;
        }
    }

    

    class Program
    {
        public static void EffCheck (ref Vehicle a)
        {
            if (a.modelYear < 1990)
            {
                a.efficiency = 10;
            } else if (a.modelYear < 2010)
            {
                a.efficiency = 50;
            } else
            {
                a.efficiency = 100;
            }
        }

        

        public static void Init (int num, out int param1, out int param2, out int param3, out int param4, out int param5)
        {
            param1 = num * 2;
            param2 = num * 10;
            param3 = num * 100;
            param4 = num * 1000;
            param5 = num * 10000;
        }


        static void Main(string[] args)
        {

            Vehicle tesla = new Vehicle
            {
                name = "ModelS",
                modelYear = 2015,
                efficiency =  0
            };

            
            Console.WriteLine("Ennen EffCheck kutsua: " + tesla);
            Console.WriteLine("");
            EffCheck(ref tesla);
            Console.WriteLine("EffCheck metodin jälkeen: " + tesla);
            Console.WriteLine("");

            int num = 5;

            Init(num, out int param1, out int param2, out int param3, out int param4, out int param5);
            Console.WriteLine($"param1={param1}; param2={param2}; param3={param3}; " +
                  $"param4={param4}; param5={param5}");
            Console.ReadKey();
        }

        
        
    }
}
