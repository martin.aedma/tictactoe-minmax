﻿using System;

namespace Harj09

{ public enum WeekDays
    {
        monday = 1,
        tuesday,
        wendsday,
        thursday,
        friday,
        saturday,
        sunday
    }
    
  public struct Car
    {
        public string manufacturer;
        public string type;
        public int year;
        public int price;

        public override string ToString()
        {
            return manufacturer + ", " + type + ", " + year + ", " + price;
        }

    }
   
    class Program
    {
        static void Main(string[] args)
        {


            WeekDays day = WeekDays.friday;
            switch (day)
            {
                case WeekDays.monday:
                    Console.WriteLine("monday is number " + (int)(WeekDays.monday) + " day of the week");
                    break;
                case WeekDays.tuesday:
                    Console.WriteLine("tuesday is number " + (int)(WeekDays.tuesday) + " day of the week");
                    break;
                case WeekDays.wendsday:
                    Console.WriteLine("wendsday is number " + (int)(WeekDays.wendsday) + " day of the week");
                    break;
                case WeekDays.thursday:
                    Console.WriteLine("thursday is number " + (int)(WeekDays.thursday) + " day of the week");
                    break;
                case WeekDays.friday:
                    Console.WriteLine("friday is number " + (int)(WeekDays.friday) + " day of the week");
                    break;
                case WeekDays.saturday:
                    Console.WriteLine("saturday is number " + (int)(WeekDays.saturday) + " day of the week");
                    break;
                case WeekDays.sunday:
                    Console.WriteLine("sunday is number " + (int)(WeekDays.sunday) + " day of the week");
                    break;
            }

            Car number1 = new Car
            {
                manufacturer = "Toyota",
                type = "Avensis",
                year = 2016,
                price = 20000
            };

            Car number2 = new Car
            {
                manufacturer = "Volvo",
                type = "S70",
                year = 2004,
                price = 3000
            };

            Car number3 = new Car
            { 
                manufacturer = "Pontiac",
                type = "Firebird",
                year = 1985,
                price = 25000
            };

            Console.WriteLine("");
            Console.WriteLine(number1);
            Console.WriteLine("");
            Console.WriteLine(number2);
            Console.WriteLine("");
            Console.WriteLine(number3);

            Console.ReadKey();
        }

        
    }
}

