This was my very first project in University, made for course Programming Basics with C#.
I wrote a simple console based TicTacToe game where computer utilizes Min-Max algorithm so it will never lose.
UI is a rough and code readability, especially in Main function, could be improved much.

Game is in Code -> Harjoitustyo

Repository also contains some course home assignments.
